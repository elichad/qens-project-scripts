import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from tkinter import filedialog
from bumps.names import Curve, FitProblem
from bumps.fitters import fit
from bumps.formatnum import format_uncertainty
import QENSmodels

import utils_
import calibrate

#filename=filedialog.askopenfilename(title='Open file to fit')

#these two files must use the same w bins
filename_calibration="../mcstas/ILL_IN5_adapted_20200115_145309/M_single_inc_1579096392.th_E" #vanadium 100m
filename="../mcstas/ILL_IN5_adapted_20200115_145309/M_single_inc_1579096392.th_E" #water 100m 

q_step_width = 0.1 #round q to nearest q_step_width
w_cal, calibration = calibrate.calibrate(filename_calibration, q_step_width, recalibrate=False)

print("Starting fitting with file {}".format(filename))

#load E_th file
info = utils_.parse_E_th_file(filename)

#retrieve instrument/sample information
instrument_name = os.path.splitext(info["Instrument"])[0]
try:
    sample_name = utils_.get_name_from_path(info["Param"]["inc"])
except KeyError:
    sample_name = utils_.get_name_from_path(info["Param"]["Sqw_inc"])  
os.makedirs(instrument_name, exist_ok=True)

#parse I(E,th) data
I_data = utils_.generate_df_from_E_th_data(info)

x_label = info['xlabel']
y_label = info['ylabel']
z_label = info['zlabel']
err_label=z_label+'_err'

longitude_label = x_label
energy_label = y_label
q_label = "Momentum transfer [1/AA]"
q_rounded_label = "q rounded"
w_label = "Energy transfer [meV]"
sqw_label="S(q,w)"

#E_in = float(info['Param']['E0']) #if E0 appears in the data file
E_in = 3.12058 #meV - hardcoded from McStas output for lambda = 5.12 Angstrom
    
#convert to I(q,w)
I_data = utils_.convert_to_q_w(I_data, E_in, longitude_label, energy_label, q_label, w_label)
I_data = utils_.round_q(I_data, q_label, q_step_width, q_rounded_label)
#symmetrise w to avoid convolution issues
I_data = utils_.symmetrize_w(I_data, w_label)

#generate I(q,w) table
I_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label, values=z_label)
I_pivot = I_pivot.fillna(value=0)

#generate err(q,w) table
err_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label, values=err_label)
err_pivot = err_pivot.fillna(0)

#model for fit - using H2O model with Lorentzians in w
#convolve with 'resolution' to account for instrument resolution
def model_convol(w, q, scale=1, center=0, D=1, resTime=1, resolution=None):
    model = QENSmodels.sqwJumpTranslationalDiffusion(w, q, scale, center, D, resTime)
    return np.convolve(model, resolution/resolution.sum(), mode='same')

#set up fitting problem
M = []
D=0.23
resTime=1.25
#choose w indices close to w=0
# - we assume there is a peak here and use this to scale the guess
# - but there can be some noise, so average over multiple indices
indices_to_avg = list(np.where(I_pivot.columns.values<0)[0][-1:]) + list(np.where(I_pivot.columns.values==0)[0]) + list(np.where(I_pivot.columns.values>0)[0][:1])
for i in range(I_pivot.index.size):
    #bumps fitting model - Lorentzian
    q = I_pivot.index[i]
    hwhm_w = D*q*q/(1+resTime*D*q*q)
    vals = np.array(I_pivot.values[i,:])
    #calculate average height at centre
    scale_val_centre = np.mean(vals[indices_to_avg])
    #bumps finds it difficult to do the fitting if some errors are very small or 0
    # - fit only points where the error is above a certain size
    non_zero = np.where(err_pivot.values[i,:]>=1e-5)[0]

    #select central part of calibration array for convolution
    # - size to equal the size of non_zero
    l_nz = len(non_zero)
    l_cal = len(calibration)
    low=0
    high=len(calibration)
    if l_nz % 2 == 0: # l_nz even
        l_half = np.floor(l_cal/2)
        low = l_half - l_nz/2 +1
        high = l_half + l_nz/2 + 1
    else: # l_nz odd
        l_half = np.ceil(l_cal/2)
        low = l_half - (l_nz-1)/2
        high = l_half + (l_nz-1)/2 + 1
    cal_select = calibration[int(low):int(high)]

    #make sure starting guess for HWHM isn't too narrow
    if hwhm_w<0.05:
        hwhm_w = 0.05
    #calculate guess for scale
    scale_test = scale_val_centre*np.pi*abs(hwhm_w)
    
    Mq = Curve(model_convol, I_pivot.columns.values[non_zero], I_pivot.values[i,non_zero], err_pivot.values[i,non_zero], q=I_pivot.index[i], scale=scale_test, center=0.0, D=0.23, resTime=1.25, resolution=cal_select)
    Mq.scale.range(min(scale_test/10,0),max(scale_test*10,1e-10))
    Mq.center.range(-0.1,0.1)
    Mq.D.range(0.01,1)
    Mq.resTime.range(0.01,5)

    #q-independent parameters
    if i==0:
        qD = Mq.D
        qT = Mq.resTime
    else:
        Mq.D = qD
        Mq.resTime = qT

    M.append(Mq)

problem = FitProblem(M)
minimizer = 'lm'
steps = 100

print('Initial chisq: ', problem.chisq_str())

#fit problem
result = fit(problem, method=minimizer, steps=steps, verbose=True)

print("Final chisq: ", problem.chisq_str())

#plot subset of 1D fits
plt.figure()
w_range = I_pivot.columns.values
sqw = []
k=-1
sigmas =[]

for i in range(I_pivot.index.size):
    qi = I_pivot.index[i]
    k += 1
    curve = M[k]
    z_vals = curve.theory()
    w_range = curve.x

    #save S(q,w) fit
    for j in range(len(z_vals)):
        sqw.append([qi,w_range[j],z_vals[j]])
    
    #plot a subset of curves
    if i%10==0:
        plt.plot(w_range,z_vals,label="q={:.2f}".format(qi))

plt.legend()
plt.savefig(instrument_name+"/"+sample_name+"_fits.png",dpi=300)

#plot 2D fit
sqw_out = pd.DataFrame(sqw,columns=[q_label,w_label,sqw_label])
utils_.plot_2d_from_dataframe(sqw_out, w_label, q_label, sqw_label, filename=instrument_name+"/"+sample_name+"_output_sqw.png")

#compare to original S(q,w) 

#load original S(q,w)
sqw_in = utils_.parse_sqw(sample_name+".sqw", q_label, w_label, sqw_label)
q_max_output = max(I_pivot.index.values)
sqw_in = sqw_in[sqw_in[q_label]<=q_max_output]
sqw_in_piv = sqw_in.pivot_table(index=q_label, columns=w_label, values=sqw_label)
sqw_in_piv = sqw_in_piv.fillna(0)

#normalise to the same value
norm_in = np.trapz(np.trapz(sqw_in_piv.values,sqw_in_piv.columns.values,axis=1), sqw_in_piv.index.values)
norm_out = np.trapz(np.trapz(I_pivot.values,I_pivot.columns.values,axis=1), I_pivot.index.values)

scale_factor = norm_in/norm_out
for i in range(I_pivot.index.size):
    for j in range(I_pivot.columns.size):
        I_pivot.values[i,j] *= scale_factor
        
#plot I(w|q) for input compared to fit
q_desired = [0.25,0.75,1.5,2.2]
fig, axes = plt.subplots(len(q_desired),1,sharex=True,figsize=(10,1.3*len(q_desired)+2))
xlim_display = [-0.5,0.5]

q_lines=[]
for i in range(len(q_desired)):
    q = q_desired[i]
    lines = []
    data_labels = ["Input", "Fitted output"]
    ax=axes[i]
    for piv in [sqw_in_piv, I_pivot]:
        q_index = np.where(piv.index.values>=q)[0][0] #find q bin
        line = ax.plot(piv.columns.values, piv.values[q_index,:])[0]
        lines.append(line)
    ax.set_xlim(xlim_display)
    ax.annotate("q={}".format(round(q,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(w_label)
fig.text(0.075,0.5,"Intensity",ha='center',va='center',rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=3, bbox_to_anchor=[0.5,0.975])

plt.savefig(instrument_name+"/"+sample_name+"_fit_comparison.png",dpi=300)

plt.show()


