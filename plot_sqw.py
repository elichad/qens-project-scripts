#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Plot and compare one or more S(q,w) distributions from .sqw files.

Plots S(q,w); probability distributions P(w) and P(q|w); and the full probability
distribution P(q,w) with the sampling restrictions currently used in Isotropic_Sqw
(as of 31/01/2020).
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import h5py
import os

import utils_

q_label = 'q [1/AA]'
w_label = 'hbar*w [meV]'
sqw_label = 'S(q,w)'

models = {
    "Ideal qmax=2.5" : {"filename" : "water_2_5.sqw"},
    "Ideal qmax=3" : {"filename" : "water_3.sqw"},
    "Ideal qmax=3.5" : {"filename" : "water_3_5.sqw"},
    "Ideal qmax=5" : {"filename" : "water_5.sqw"},
    "Ideal qmax=10" : {"filename" : "water_10.sqw"},
#    "Ideal qmax=100" : {"filename" : "water_100.sqw"},
#    "Experimental" : {"filename" : "water_hdf.sqw"},
#    "McStas" : {"filename" : "water_mcstas.sqw"},
}
          
for model in models.keys():
    filename = models[model]["filename"]
    print("Reading file {}".format(filename))

    sqw = utils_.parse_sqw(filename, q_label, w_label, sqw_label)
    sample_name = utils_.get_name_from_path(filename)

    utils_.plot_2d_from_dataframe(sqw, w_label, q_label, sqw_label, sample_name+"_"+model+".png", title="S(q,w) "+model)

    #calculate |S|
    piv = sqw.pivot_table(index=q_label, columns=w_label, values=sqw_label)
    piv = piv.fillna(0)
    norm=np.trapz(np.trapz(piv.values,piv.columns,axis=1),piv.index)
    models[model]["norm"] = norm
    models[model]["data"] = piv

"""
#normalise to same scale - useful for external source .sqw files
norm_to = models[list(models.keys())[1]]["norm"]
for model in models.keys():
    data = models[model]["data"]
    #normalise
    scale_by = norm_to/models[model]["norm"]
    print("scaling {} by {}".format(model,scale_by))
    for ind,val in np.ndenumerate(data.values):
        data.values[ind] = val*scale_by 
"""

#get probability distributions
print("Calculating probability distributions")
for model in models.keys():
    data = models[model]["data"]
    qxdata = data.copy()
    #generate q*S(q,w)
    for ind,val in np.ndenumerate(data.values):
        qxdata.values[ind] = val*data.index[ind[0]]
    #P(w)dw = \int q*S(q,w) dq / |S|
    Pw = np.trapz(qxdata.values, qxdata.index.values, axis=0)/models[model]["norm"]
    #\int P(w) dw = 1
    norm_pw = np.trapz(Pw, qxdata.columns.values)
    Pw = Pw/norm_pw

    models[model]["qxdata"] = qxdata
    models[model]["Pw"] = Pw

comparison_path="comparisons/"
os.makedirs(comparison_path, exist_ok=True)
# PLOTTING
print("Generating plots")

# plot direct comparison of S(q,w)s

q_choose = [0.5,1,1.5,2,2.5,5,9.5] #q values to visualise
ncol = len(q_choose) if len(q_choose)<=4 else 3
fig, axes = plt.subplots(len(q_choose), 1, sharex=True, figsize=(10,1.3*len(q_choose)+2))
xlim_display = [-1,1]

for i in range(len(q_choose)):
    q = q_choose[i]
    ax = axes[i]
    if i==0:
        lines = []
        data_labels = []
    for model in models.keys():
        data = models[model]["data"]
        #select and plot appropriate q
        try:
            q_index = np.where(data.index.values >= q)[0][0]
            line = ax.plot(data.columns.values, data.values[q_index,:])[0]
        except IndexError:
            line = ax.plot(data.columns.values, [0]*len(data.columns.values))
        if i==0: #for labelling purposes
            lines.append(line)
            data_labels.append(model)
    ax.set_xlim(xlim_display)
    #ax.set_ylim(ylim_display)
    ax.annotate("q={}".format(round(q,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(w_label)
fig.text(0.075, 0.5, "S(q,w)", ha='center', va='center', rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=ncol, bbox_to_anchor=[0.5,0.95])
fig.suptitle("S(q,w) distributions")

plt.savefig(comparison_path+"sqw_model_comparison.png",dpi=300)

# plot probability distributions

figw,axesw = plt.subplots(1,1,figsize=(10,6))
xlim_display_w = [-2,2]

ax = axesw
for model in models.keys():
    data = models[model]["data"]
    Pw = models[model]["Pw"]
    line = ax.plot(data.columns.values,Pw,label=model)[0]
ax.set_xlim(xlim_display_w)
ax.set_xlabel(w_label)
ax.set_ylabel("P(w)")
figw.suptitle("P(w) distributions")
figw.legend(loc="upper center", borderaxespad=0.1, ncol=ncol, bbox_to_anchor=[0.5,0.95])

plt.savefig(comparison_path+"sqw_models_Pw.png",dpi=300)

w_choose = [0,0.25,0.5,1.5]
ncol = len(w_choose) if len(w_choose)<=4 else 3
figq, axesq = plt.subplots(len(w_choose), 1, sharex=True, figsize=(10,1.3*len(w_choose)+2))
xlim_display_q = [0,10]

for i in range(len(w_choose)):
    w = w_choose[i]
    ax = axesq[i]
    if i==0:
        lines = []
        data_labels = []
    for model in models.keys():
        data = models[model]["data"]
        qxdata = models[model]["qxdata"]
        #calculate and plot Pq
        Sq = np.trapz(data.values, data.columns.values, axis=1)
        w_index = np.where(data.columns.values >= w)[0][0]
        Pq = qxdata.values[:,w_index]/Sq
        norm_Pq = np.trapz(Pq, data.index.values)
        Pq = Pq/norm_Pq
        line = ax.plot(data.index.values, Pq)[0]
        if i==0:
            lines.append(line)
            data_labels.append(model)
    ax.set_xlim(xlim_display_q)
    #ax.set_ylim(ylim_display)
    ax.annotate("w={}".format(round(w,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axesq[-1].set_xlabel(q_label)
figq.text(0.075, 0.5, "P(q|w)", ha='center', va='center', rotation='vertical')
figq.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=ncol, bbox_to_anchor=[0.5,0.95])
figq.suptitle("P(q|w) distributions")

plt.savefig(comparison_path+"sqw_models_Pq.png",dpi=300)

# P(q,w) given upper limits on index_w and index_q from Isotropic_Sqw

for model in models.keys():
    Ei = 3.12 #meV - mean from McStas output
    SE2V = 437.393 
    V2K = 0.00158825
    data = models[model]["data"]
    qxdata = models[model]["qxdata"]
    w_max = max(data.columns.values)
    w_lim_ind = int(np.floor((1+Ei/w_max)/2 * len(data.columns)))
    Pw = models[model]["Pw"].copy()
    for i in range(w_lim_ind,len(data.columns)):
        Pw[i]=0
    Pw = Pw/np.trapz(Pw,data.columns.values)
    Sq = np.trapz(data.values, data.columns.values, axis=1)
    q_max = max(data.index.values)
    Pqw = data.copy()
    for i in range(len(data.columns.values)):
        w = data.columns[i]
        if w<=Ei:
            q_lim_ind = int(np.floor(SE2V * V2K * (np.sqrt(Ei) + np.sqrt(Ei-w))/q_max * len(data.index)))
        else:
            q_lim_ind = int(np.floor(2 * SE2V * V2K * np.sqrt(Ei)/q_max * len(data.index)));
        Pq = qxdata.values[:,i].copy()/Sq
        for j in range(q_lim_ind+1,len(data.index)):
            Pq[j]=0
        Pq = Pq/np.trapz(Pq,data.index.values)
        Pqw.values[:,i] = Pq*Pw[i]

    fig,ax=plt.subplots()
    xy_limits_for_plot = [min(Pqw.columns), max(Pqw.columns), max(Pqw.index), min(Pqw.index)]
    im = ax.imshow(Pqw, aspect='auto', extent=xy_limits_for_plot)
    ax.invert_yaxis() #necessary in this context
    ax.set_xlabel(w_label)
    ax.set_ylabel(q_label)
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("P(q,w)", rotation=-90, va="bottom")
    ax.set_title("P(q,w) selection for {}".format(model))
    plt.savefig(sample_name+"_"+model+"_Pqw.png",dpi=300)
plt.show()
