# qens-project-scripts

### Requirements

Python 3.7 64-bit installation (a 32-bit installation may throw memory errors)

Packages from `requirements.txt`

[QENSmodels](https://github.com/QENSlibrary/QENSmodels) package, which must be installed manually

### Generating an S(q,w)

The `generate_sqw.py` file generates `.sqw` files using [QENSmodels](https://github.com/QENSlibrary/QENSmodels) functions. The `plot_sqw.py` creates plots with one or more of these .sqw files for comparison purposes - including direct comparisons and comparisons of the probability distributions used to select w and q values in the `Isotropic_Sqw` McStas component.

The `plot_sqw.py` script allows comparison of different `.sqw` files.

### Running a McStas simulation

The file `ILL_IN5_adapted.instr` is an adapted version of the `ILL_IN5` McStas template. The adaptations add new detectors to distinguish between single and multiple scattering events. This instrument also uses the `Isotropic_Sqw_edit` component (C file included in this repo) which removes the statistical bug in the selection of (w,q) samples in the original `Isotropic_Sqw` component.

Simulations can be run with the default values, except for the `inc` parameter, where you should enter a `.sqw` file (e.g. one produced by `generate_sqw.py`).

### Plotting the Output

The script `plot_output.py` will compare different output files from the *same* McStas run - e.g. to compare the single, multiple and total scattering contributions.

To compare output files from multiple different runs, use `plot_output_comparison.py`.

### Fitting

The files `calibrate.py` and `fit_output.py` were developed for fitting the McStas output using the `bumps` package. The method used in both is very similar. It was difficult to get a good fit output from these scripts, so more work is probably needed on these.