#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Find the resolution of an instrument based on McStas simulation data.

"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from tkinter import Tk,filedialog
from bumps.names import Curve, FitProblem
from bumps.fitters import fit
from bumps.formatnum import format_uncertainty
import QENSmodels

import utils_

def calibrate(filename,step_width,recalibrate=False):
    """Determine the resolution (in w) of an instrument, given E-theta data 
from a McStas simulation.
    
    """
    #load E-th file
    info = utils_.parse_E_th_file(filename)
    
    #retrieve instrument/sample information
    instrument_name = os.path.splitext(info["Instrument"])[0]
    try:
        sample_name = utils_.get_name_from_path(info["Param"]["inc"])
    except KeyError:
        sample_name = utils_.get_name_from_path(info["Param"]["Sqw_inc"])   

    suffix = '_'.join(str(step_width).split('.'))
    
    #if not recalibrating, try to find a previous calibration for the same sample
    if not recalibrate:
        try:
            with open(instrument_name+'/'+'calibration_'+sample_name+'_'
                      +suffix+'.dat','r') as f:
                w = f.readline()
                calibration = f.readline()
                w = [float(wi.strip()) for wi in w.split(' ')]
                calibration = [float(ci.strip()) for ci in calibration.split(' ')]
            return np.array(w), np.array(calibration)
        except FileNotFoundError:
            print("No file found for previous calibration with this step width or sample. Calibrating from scratch.")
    
    #calibrate from scratch
    
    #parse I(E,th) data
    I_data = utils_.generate_df_from_E_th_data(info)

    x_label = info['xlabel']
    y_label = info['ylabel']
    z_label = info['zlabel']
    err_label=z_label+'_err'

    longitude_label = x_label
    energy_label = y_label
    q_label = "Momentum transfer [1/AA]"
    q_rounded_label = "q rounded"
    w_label =  "Energy transfer [meV]"
    sqw_label="S(q,w)"

    #E_in = float(info['Param']['E0']) #if E0 appears in the data file
    E_in = 3.12058 #meV - hardcoded from McStas output for lambda = 5.12 Angstrom
    
    #convert to I(q,w)
    I_data = utils_.convert_to_q_w(I_data, E_in, longitude_label, energy_label, q_label, w_label)
    I_data = utils_.round_q(I_data, q_label, step_width, q_rounded_label)
    I_data = utils_.symmetrize_w(I_data, w_label)

    #plot I(q,w)
    utils_.plot_2d_from_dataframe(I_data, w_label, q_rounded_label, z_label, fillna=0, filename=instrument_name+'_'+sample_name+'_output_q-w.png')

    #generate I(q,w) table
    I_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label, values=z_label)
    I_pivot = I_pivot.fillna(value=0)
    
    #generate err(q,w) table
    err_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label, values=err_label)
    fill_value = np.nanmax(err_pivot.values)/1000
    err_pivot = err_pivot.fillna(value=fill_value)
    err_pivot=err_pivot.where(err_pivot>=fill_value, other=fill_value)
    
    #model to use for fit
    def model_calibration(w, scale=1, center=0, sigma=1):
        model = QENSmodels.gaussian(w, scale, center, sigma)
        return model

    #set up fitting problem
    M = []
    for i in range(I_pivot.index.size):
        #bumps fitting model - Gaussian
        val_test = I_pivot.values[i,:]
        if np.any(val_test):
            hw_w = I_pivot.columns[np.where(val_test>max(val_test)/2)[0][-1]] #assuming distribution is centred on/very close to 0
            hw_w = abs(hw_w)
        else:
            hw_w = 0.1
        scale_test = np.amax(val_test)*np.sqrt(2*np.pi)*hw_w

        Mq = Curve(model_calibration, I_pivot.columns.values, I_pivot.values[i,:], err_pivot.values[i,:], scale=scale_test, center=0.0, sigma=hw_w)

        Mq.scale.range(0,scale_test*100 if scale_test>0 else 1e-10)
        Mq.center.range(-0.1,0.1)
        Mq.sigma.range(0,5)
        
        M.append(Mq)

    problem = FitProblem(M)
    minimizer = 'lm'
    steps = 100
    
    print('Initial chisq: ', problem.chisq_str())

    #fit problem
    result = fit(problem, method=minimizer, steps=steps, verbose=True)

    print("Final chisq: ", problem.chisq_str())

    #normalise curves to unity + plot a subset; recover HWHM values
    plt.figure()
    w_range = I_pivot.columns.values
    sqw = []
    k=-1
    sigmas =[]
    for i in range(I_pivot.index.size):
        qi = I_pivot.index[i]
        k += 1
        curve = M[k]
        z_vals = curve.theory()

        # normalise + save HWHM
        if np.any(z_vals):
            norm_scale = np.trapz(z_vals, w_range)
            z_vals = z_vals/norm_scale
            sigmas.append(curve.sigma.value)
        
        #save S(q,w) fit
        for j in range(len(z_vals)):
            sqw.append([qi,w_range[j],z_vals[j]])
        
        if i%10==0:
            plt.plot(w_range,z_vals,label="q={:.2f}".format(qi))

    plt.legend()
    os.makedirs(instrument_name, exist_ok=True)
    plt.savefig(instrument_name+'/'+sample_name+"_fits_calibration.png",dpi=300)

    #find average HWHM
    sigma_avg = np.mean(sigmas)
    print("Average final HWHM: ",sigma_avg)
    #generate Gaussian with this average HWHM
    calibration_model = QENSmodels.gaussian(I_pivot.columns.values, scale=1, 
                                            center=0, sigma=sigma_avg)
    calibration_model = np.array(calibration_model)

    #write calibration model to file
    suffix = '_'.join(str(step_width).split('.'))
    os.makedirs(instrument_name, exist_ok=True)
    with open(instrument_name+'/'+"calibration_"+sample_name+"_"+suffix+".dat",
              "w") as f_out:
        f_out.write(" ".join([str(w) for w in w_range])+"\n")
        f_out.write(" ".join([str(cm) for cm in calibration_model])+"\n")
    
    print("Calibration completed")
    return np.array(I_pivot.columns.values), np.where(calibration_model>0, calibration_model, 1e-15)

if __name__=="__main__":
    step_width = 0.1
    root=Tk()
    root.withdraw()
    filename=filedialog.askopenfilename(title='Open file to fit')
    calibrate(filename, step_width,recalibrate=True)
    root.destroy()
    plt.show()

