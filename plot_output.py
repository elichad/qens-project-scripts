#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Plot results of a single McStas simulation.

Intended for selection of multiple detector outputs from a single McStas run, 
which share the same bins and are in the same folder.
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from tkinter import Tk, filedialog
import time

import utils_

root=Tk()
root.withdraw()

start_total = time.time()

#select files - these must all use the same bins
filenames = list(filedialog.askopenfilenames(initialdir="~/Documents/mcstas/", title='Select file(s) to plot'))

q_step_width = 0.1 #AA^-1
q_rounded_label = "q rounded"

all_data={}
for filename in filenames:
    print("Loading file {}".format(filename))
    detector = utils_.get_name_from_path(filename)
    #parse E_th file
    info = utils_.parse_E_th_file(filename)    
    I_data = utils_.generate_df_from_E_th_data(info)

    #assume all files are from the same folder and contain the same fields; 
    #initialise based on this
    if filename == filenames[0]:
        x_label = info["xlabel"]
        y_label = info["ylabel"]
        z_label = info["zlabel"]
        err_label = z_label+"err"
        n_label = "Number of events"

        try:
            sample_filename = info["Param"]["inc"]
        except KeyError:
            sample_filename = info["Param"]["Sqw_inc"]
        sample_name = utils_.get_name_from_path(sample_filename)
        instrument_name = os.path.splitext(info["Instrument"])[0]
        
        base_path = os.path.join(instrument_name, sample_name)
        base_name = os.path.join(base_path,sample_name)
        os.makedirs(base_path, exist_ok=True)

        theta_label = x_label
        energy_label = y_label
        
        q_label = "Momentum transfer [1/AA]"
        w_label = "Energy transfer [meV]"
        
        E_in = 3.12058 #meV - hardcoded from McStas output for lambda = 5.12 Angstrom
        #E_in = float(info["Param"]["E0"]) #if E0 appears in the output files
        
    detector_base_name = base_name + "_" + detector    

    #generate I(E,theta) table
    I_Eth_pivot = I_data.pivot_table(index=theta_label, columns=energy_label,
                                     values=z_label, aggfunc=np.mean)
    I_Eth_pivot = I_Eth_pivot.fillna(0)

    #convert to I(q,w) data
    I_data = utils_.convert_to_q_w(I_data, E_in, theta_label, energy_label,
                                   q_label, w_label)
    I_data = utils_.round_q(I_data, q_label, q_step_width, q_rounded_label)
    I_data = utils_.symmetrize_w(I_data, w_label)

    #generate I(q,w) table
    I_qw_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label, 
                                    values=z_label, aggfunc=np.mean)
    I_qw_pivot = I_qw_pivot.fillna(0)
    
    #calculate norm of I(q,w)
    norm_qw = np.trapz(np.trapz(I_qw_pivot.values,I_qw_pivot.columns.values,axis=1),
                       I_qw_pivot.index.values)
    
    #store data
    all_data[detector] = {"df" : I_data, "piv_Eth" : I_Eth_pivot, 
                          "piv_qw" : I_qw_pivot, "norm_qw" : norm_qw}

    print("Generating individual plots for {}".format(detector))

    #plot I(E,theta)
    utils_.plot_2d_from_dataframe(I_data, theta_label, energy_label, z_label,
                                  filename=detector_base_name+"_output_longitude-energy.png",
                                  title="I(E,theta) for "+detector)

    #plot I(E|theta)
    fig = plt.figure()
    n_lines = 4
    interval = np.floor(I_Eth_pivot.index.size/(n_lines)-1)
    for i in range(I_Eth_pivot.index.size):
        if i%interval==0:
            theta = I_Eth_pivot.index[i]
            plt.plot(I_Eth_pivot.columns.values,I_Eth_pivot.values[i,:],
                     label="theta={}".format(round(theta)))
    plt.xlabel(energy_label)
    plt.xlim((E_in-1,E_in+1))
    plt.ylabel("Intensity")
    plt.title("I(E) for "+detector)
    plt.legend()
    fig.savefig(detector_base_name+"_I-E.png",dpi=300)

    #plot I(theta)
    I_theta = np.trapz(I_Eth_pivot.values, I_Eth_pivot.columns.values, axis=1)
    fig=plt.figure()
    plt.plot(I_Eth_pivot.index.values,I_theta)
    plt.xlabel(theta_label)
    plt.ylabel("Intensity")
    plt.title("I(theta) for "+detector)
    fig.savefig(detector_base_name+"_I-th.png",dpi=300)

    #plot I(q,w)
    utils_.plot_2d_from_dataframe(I_data, w_label, q_rounded_label, z_label,
                                  filename=detector_base_name+"_output_q-w.png",
                                  fillna=0, title="I(q,w) for "+detector)

sample_filename = sample_name+".sqw" #placeholder to get my code to run
print("Loading original input S(q,w) from {}".format(sample_filename))

sqw_label = "S(q,w)"

sqw_in = utils_.parse_sqw(sample_filename, q_label, w_label, sqw_label)
sqw_in = utils_.round_q(sqw_in, q_label, q_step_width, q_rounded_label)

#generate S(q,w) table
sqw_in_piv = sqw_in.pivot_table(index=q_rounded_label, columns=w_label, 
                                values=sqw_label, aggfunc=np.mean)
sqw_in_piv = sqw_in_piv.fillna(0)

#calculate norm of S(q,w)
norm_sqw_in = np.trapz(np.trapz(sqw_in_piv.values,sqw_in_piv.columns.values,axis=1),
                       sqw_in_piv.index.values)

#store with other data
all_data[sqw_label] = {"df" : sqw_in, "piv_qw" : sqw_in_piv, 
                       "norm_qw" : norm_sqw_in}

print("Normalising datasets to a comparable scale")

#normalise based on the norm of S(q,w) - but this does not always work well 
#result depends on the w and q limits of the .sqw file compared to the data files
for detector in all_data.keys():
    scale = all_data[sqw_label]["norm_qw"]/all_data[detector]["norm_qw"]
    all_data[detector]["scale_qw"] = scale
    piv = all_data[detector]["piv_qw"]
    for ind,val in np.ndenumerate(piv.values):
        piv.values[ind] = val*scale

#PLOTTING
print("Generating comparison plots")

#plot I(w|q) for all datasets
q_desired = [0.25,0.75,1.5,2.5]
fig, axes = plt.subplots(len(q_desired),1,sharex=True,sharey=False,
                         figsize=(10,1.3*len(q_desired)+2))
xlim_display = [-1,1]

q_lines=[]
for i in range(len(q_desired)):
    q = q_desired[i]
    lines = []
    data_labels = []
    ax=axes[i]
    for detector in all_data.keys():
        piv = all_data[detector]["piv_qw"]
        q_index = np.where(piv.index.values>=q)[0][0] #find q bin
        line = ax.plot(piv.columns.values, piv.values[q_index,:])[0]
        lines.append(line)
        data_labels.append(detector)
    ax.set_xlim(xlim_display)
    ax.annotate("q={}".format(round(q,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(w_label)
fig.text(0.075,0.5,"Intensity",ha='center',va='center',rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=3, bbox_to_anchor=[0.5,0.975])

plt.savefig(base_name+"_w_given_q.png",dpi=300)

#plot I(E|th) for all datasets

th_desired = [5,60,100,170]
fig, axes = plt.subplots(len(th_desired),1,sharex=True,sharey=False,
                         figsize=(10,1.3*len(q_desired)+2))
xlim_display = [2.8,3.4] #energy limits

th_lines=[]
for i in range(len(th_desired)):
    th = th_desired[i]
    lines = []
    data_labels = []
    ax=axes[i]
    for detector in all_data.keys():
        if detector == sqw_label:
            continue
        piv = all_data[detector]["piv_Eth"]
        th_index = np.where(piv.index.values>=th)[0][0] #find theta bin
        line = ax.plot(piv.columns.values,piv.values[th_index,:])[0]
        lines.append(line)
        data_labels.append(detector)
    ax.set_xlim(xlim_display)
    ax.annotate("theta={}".format(round(th,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(energy_label)
fig.text(0.075,0.5,"Intensity",ha='center',va='center',rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=3, bbox_to_anchor=[0.5,0.975])

plt.savefig(base_name+"_E_given_th.png",dpi=300)

root.destroy()
print("Program complete. Total runtime: {} s".format(time.time()-start_total))
plt.show()