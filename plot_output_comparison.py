#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Compare results of multiple McStas simulations.

Does not require the data files to use the same bins, but plots assume that the
same data types are present in all files.

"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from tkinter import filedialog
import time

import utils_

start_total = time.time()

all_data = {
#    "Ideal q_max=2.5" : {"filename" :"../mcstas/ILL_IN5_adapted_20200115_153540/M_single_inc_1579098943.th_E"},
#    "Ideal q_max=3" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_145309/M_single_inc_1579096392.th_E"},
#    "Ideal q_max=3.5" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_145247/M_single_inc_1579096370.th_E"},
#    "Ideal q_max=5" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_145228/M_single_inc_1579096351.th_E"},
    "Ideal q_max=10" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_145209/M_single_inc_1579096332.th_E"},
    "Experimental" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_153546/M_single_inc_1579098947.th_E"},
    "McStas" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_153557/M_single_inc_1579098960.th_E"},
#    "Original" : {"filename" : "../mcstas/ILL_IN5_adapted_20200115_145209/M_single_inc_1579096332.th_E"},
#    "q factors removed" : {"filename" : "../mcstas/ILL_IN5_test_20200129_100615/M_single_inc_1580288778.th_E"},
#    "index_q limit removed" : {"filename" : "..mcstas/ILL_IN5_test_20200129_121044/M_single_inc_1580296247.th_E"},
#    "Both" : {"filename" : "../mcstas/ILL_IN5_test_20200129_104246/M_single_inc_1580290969.th_E"},
}

q_step_width = 0.1
q_rounded_label = "q rounded"
q_label = "Momentum transfer [1/AA]"
w_label = "Energy transfer [meV]"
 
for model in all_data.keys():
    filename = all_data[model]["filename"]
    print("Loading file {}".format(filename))
    run_name = utils_.get_name_from_path(filename)
    #parse E_th file
    info = utils_.parse_E_th_file(filename)    
    I_data = utils_.generate_df_from_E_th_data(info)

    x_label = info["xlabel"]
    y_label = info["ylabel"]
    z_label = info["zlabel"]
    err_label = z_label+"err"
    n_label = "Number of events"

    try:
        sample_filename = info["Param"]["inc"]
    except KeyError:
        sample_filename = info["Param"]["Sqw_inc"]
    sample_name = utils_.get_name_from_path(sample_filename)
    instrument_name = os.path.splitext(info["Instrument"])[0]
        
    #thickness = "_".join(info["Param"]["thickness"].split("."))

    theta_label = x_label
    energy_label = y_label
    
    E_in = 3.12058 #meV - hardcoded from McStas output for lambda = 5.12 Angstrom
    #E_in = float(info['Param']['E0']) #if E0 appears in the output files
    
    #assume sample_name format of material_X_X, where X.X is the value of q_max
    base_path = os.path.join(instrument_name, sample_name)
    base_name = os.path.join(base_path,sample_name)
    os.makedirs(base_path, exist_ok=True)

    model_base_name = base_name + "_" + "_".join(model.split(" "))
    comparison_name = "comparisons/"
    
    #generate I(E,theta) table
    I_Eth_pivot = I_data.pivot_table(index=theta_label, columns=energy_label,
                                     values=z_label, aggfunc=np.mean)
    I_Eth_pivot = I_Eth_pivot.fillna(0)

    #convert to I(q,w) data
    I_data = utils_.convert_to_q_w(I_data, E_in, theta_label, energy_label, 
                                   q_label, w_label)
    I_data = utils_.round_q(I_data, q_label, q_step_width, q_rounded_label)
    I_data = utils_.symmetrize_w(I_data, w_label)
    
    #generate I(q,w) table
    I_qw_pivot = I_data.pivot_table(index=q_rounded_label, columns=w_label,
                                    values=z_label, aggfunc=np.mean)
    I_qw_pivot = I_qw_pivot.fillna(0)
    
    #calculate norm of I(q,w)
    norm_qw = np.trapz(np.trapz(I_qw_pivot.values,I_qw_pivot.columns.values,axis=1), I_qw_pivot.index.values)

    #store data
    all_data[model].update({"df" : I_data, "piv_Eth" : I_Eth_pivot,
                            "piv_qw" : I_qw_pivot, "norm_qw" : norm_qw,
                            "sqw" : sample_filename, "run_name" : run_name})
    
    ###### generate single-file plots

    print("Generating individual plots for {}".format(model))

    #plot I(E,theta)
    utils_.plot_2d_from_dataframe(I_data, theta_label, energy_label, z_label,
                                  filename=model_base_name+"_output_longitude-energy.png",
                                  title="I(E,theta) for "+model)
                                  
    #plot I(E|theta)
    fig = plt.figure()
    n_lines = 4
    interval = np.floor(I_Eth_pivot.index.size/(n_lines)-1)
    for i in range(I_Eth_pivot.index.size):
        if i%interval==0:
            theta = I_Eth_pivot.index[i]
            plt.plot(I_Eth_pivot.columns.values,I_Eth_pivot.values[i,:],
                     label="theta={}".format(round(theta)))
    plt.xlabel(energy_label)
    plt.xlim((E_in-1,E_in+1))
    plt.ylabel("Intensity")
    plt.title("I(E) for "+model)
    plt.legend()
    fig.savefig(model_base_name+"_I-E.png",dpi=300)

    #plot I(theta)
    I_theta = np.trapz(I_Eth_pivot.values, I_Eth_pivot.columns.values, axis=1)
    fig=plt.figure()
    plt.plot(I_Eth_pivot.index.values,I_theta)
    plt.xlabel(theta_label)
    plt.ylabel("Intensity")
    plt.title("I(theta) for "+model)
    fig.savefig(model_base_name+"_I-th.png",dpi=300)

    # plot I(q,w)
    utils_.plot_2d_from_dataframe(I_data, w_label, q_rounded_label, z_label, 
                                  filename=model_base_name+"_output_q-w.png",
                                  y_axlabel=q_label, fillna=0, 
                                  title="I(q,w) for "+model)
 
"""
# load an input S(q,w) - change all_data key to select the desired model
sqw_filename = all_data["Original"]["sqw"]
print("Loading input S(q,w)" from {}".format(sqw_filename))

sqw_label = "S(q,w)"

sqw_in = utils_.parse_sqw(sqw_filename, q_label, w_label, sqw_label)
sqw_in = utils_.round_q(sqw_in, q_label, q_step_width, q_rounded_label)

#generate S(q,w) table
sqw_in_piv = sqw_in.pivot_table(index=q_rounded_label, columns=w_label,
                                values=sqw_label, aggfunc=np.mean)
sqw_in_piv = sqw_in_piv.fillna(0)

#calculate norm of S(q,w)
norm_sqw_in = np.trapz(np.trapz(sqw_in_piv.values,sqw_in_piv.columns.values,axis=1),
                       sqw_in_piv.index.values)
                       
#store with other data
all_data[sqw_label] = {"df" : sqw_in, "piv_qw" : sqw_in_piv,
                       "norm_qw" : norm_sqw_in}

print("Normalising datasets to a comparable scale")

#normalise based on the norm of S(q,w) - but this does not always work well 
#result depends on the w and q limits of the .sqw file compared to the data files
for model in all_data.keys():
    scale = all_data[sqw_label]["norm_qw"]/all_data[model]["norm_qw"]
    all_data[model]["scale_qw"] = scale
    piv = all_data[model]["piv_qw"]
    for ind,val in np.ndenumerate(piv.values):
        piv.values[ind] = val*scale
"""

#PLOTTING

#plot I(w|q) for all datasets
q_desired = [0.25,0.75,1.5,2.2]
fig, axes = plt.subplots(len(q_desired),1,sharex=True,figsize=(10,1.3*len(q_desired)+2))
xlim_display = [-0.5,0.5]

q_lines=[]
for i in range(len(q_desired)):
    q = q_desired[i]
    lines = []
    data_labels = []
    ax=axes[i]
    for model in all_data.keys():
        piv = all_data[model]["piv_qw"]
        q_index = np.where(piv.index.values>=q)[0][0] #find q bin
        line = ax.plot(piv.columns.values, piv.values[q_index,:])[0]
        lines.append(line)
        data_labels.append(model)
    ax.set_xlim(xlim_display)
    ax.annotate("q={}".format(round(q,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(w_label)
fig.text(0.075,0.5,"Intensity",ha='center',va='center',rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=3, bbox_to_anchor=[0.5,0.975])

plt.savefig(comparison_name+"_I_w_given_q_comparison.png",dpi=300)

#plot I(q|w) for all datasets 
w_desired = [0, 0.25, 0.5, 1]
fig, axes = plt.subplots(len(w_desired),1,sharex=True,figsize=(10,1.3*len(w_desired)+2))
xlim_display = [0,3]

w_lines=[]
for i in range(len(w_desired)):
    w = w_desired[i]
    lines = []
    data_labels = []
    ax=axes[i]
    for model in all_data.keys():
        piv = all_data[model]["piv_qw"]
        w_index = np.where(piv.columns.values>=w)[0][0]
        line = ax.plot(piv.index.values, piv.values[:,w_index])[0]
        lines.append(line)
        data_labels.append(model)
    ax.set_xlim(xlim_display)
    ax.annotate("w={}".format(round(w,2)), xy=(0.95,0.9), xycoords='axes fraction', ha='right', va='top')

axes[-1].set_xlabel(q_label)
fig.text(0.075,0.5,"Intensity",ha='center',va='center',rotation='vertical')
fig.legend(lines, data_labels, loc="upper center", borderaxespad=0.1, ncol=3, bbox_to_anchor=[0.5,0.975])

plt.savefig(comparison_name+"_I_q_given_w_comparison.png",dpi=300)

# plot I(theta) for each dataset
fig = plt.figure()

for model in all_data.keys():
    try:
        piv = all_data[model]["piv_Eth"]
    except KeyError:
        continue
    I_theta = np.trapz(piv.values, piv.columns.values, axis=1)
    plt.plot(piv.index.values, I_theta, label=model)
plt.xlabel(theta_label)
plt.ylabel("I(theta)")
fig.legend(ncol=3)

plt.savefig(comparison_name+"_I_theta_comparison.png",dpi=300)

# plot I(w) for each dataset
fig = plt.figure()

for model in all_data.keys():
    piv = all_data[model]["piv_qw"]
    I_w = np.trapz(piv.values, piv.index.values, axis=0)
    plt.plot(piv.columns.values, I_w, label=model)
plt.xlabel(w_label)
plt.ylabel("I(w)")
fig.legend(ncol=3)

plt.savefig(comparison_name+"_I_w_comparison.png",dpi=300)

print("Program complete. Total runtime: {} s".format(time.time()-start_total))
plt.show()