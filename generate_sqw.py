#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate a .sqw file.

Includes parameters for vanadium and water, and optional code for converting 
an HDF file from LAMP to an .sqw file.

"""

import numpy as np
import matplotlib.pyplot as plt
import h5py
import pandas as pd

import QENSmodels
from QENSmodels.jump_translational_diffusion import sqwJumpTranslationalDiffusion

import utils_

# constants
hbar=6.582e-13 #meV s
THztomeV=hbar/1e-12
k=8.617333262145e-2 #eV/K

# sample details - two sets provided below
samples = { 
    "vanadium" : {
        "name" : "vanadium",
        "density" : 6.11, #g/cm^3
        "weight" : 50.94151, #g/mol
        "sigma_abs" : 0, #absorption scattering cross section in [barn]
        "sigma_coh" : 0, #coherent scattering cross section in [barn]
        "sigma_inc" : 5.08, #incoherent scattering cross section in [barn]
        "temperature" : 300, #K  
    },
    "water" : {
        "name" : "water",
        "density" : 1, #g/cm^3
        "weight" : 18, #g/mol
        "sigma_abs" : 0, #absorption scattering cross section in [barn]
        "sigma_coh" : 0, #coherent scattering cross section in [barn]
        "sigma_inc" : 160, #incoherent scattering cross section in [barn]
        "temperature" : 300, #K
    }       
}               

"""Change this line to select the sample to use"""
sample = samples["water"]

# generate S(q,w)
wlow=-24 #ps^-1
whigh=24 #ps^-1
wspacing=0.01 #ps^-1

w=np.linspace(wlow,whigh,int(np.ceil((whigh-wlow)/wspacing)+1))
q=np.linspace(0.25,2.5,1001) #AA^-1

if sample["name"] == "water":
    sqw=sqwJumpTranslationalDiffusion(w, q, scale=1, center=0, D=0.23, resTime=1.25)
elif sample["name"] == "vanadium":
    #create very narrow Gaussian to mimic a delta function
    sqw=np.zeros((q.size,w.size))
    for i in range(q.size):
       sqw[i,:]=QENSmodels.gaussian(w,1,0,wspacing/2)
else:
    raise Exception("Sample choice not recognised")

"""
#alternative code: for converting HDF file to sqw. Note that plots with this may not be labelled properly on their axes if the binning is non-linear.

info = h5py.File("water_LAMP.hdf",'r')
X = info['entry1/data1/X'][()] #w
Y = info['entry1/data1/Y'][()] #longitude
data = info['entry1/data1/DATA'][()]
err = info['entry1/data1/errors'][()]

data_list = []
for i in range(len(X)):
    for j in range(len(Y)):
        data_list.append([X[i],Y[j],data[j,i],err[j,i]])


df = pd.DataFrame(data_list,columns=["X","Y","sqw","errors"])
#manually convert to q as w is already converted
c=3e8 #m/s
m=9.39565e11 #neutron mass meV/c^2
hbar=6.582e-13 #meV s
lambda0 = 5e-10 #m
E0 = (2*c**2/m)*(np.pi*hbar/lambda0)**2
df["w"] = df["X"] #meV
df["q"] = 1e-10*np.sqrt((2*m/(c**2*hbar**2))*(2*E0+df["w"]-2*np.sqrt(E0*(E0+df["w"]))*np.cos(df["Y"]*2*np.pi/360.))) #1/AA
df = utils_.round_q(df, "q", 0.01, "q rounded")
piv = df.pivot_table(index='q rounded',columns='w',values='sqw')
piv = piv.fillna(0)
q = piv.index.values
w = piv.columns.values
sqw = piv.values
"""

# write .sqw file
with open(sample["name"]+".sqw","w") as outFile:
    outFile.write("""
#Physical parameters:
# density     {}  in [g/cm^3]
# weight      {}  in [g/mol]
# sigma_abs   {}   absorption scattering cross section in [barn]
# sigma_coh   {}   coherent scattering cross section in [barn]
# sigma_inc   {}    incoherent scattering cross section in [barn]
# Temperature {}    in [K]
# classical   1      symmetric, does not include Bose factor
""".format(sample["density"], sample["weight"], sample["sigma_abs"], sample["sigma_coh"], sample["sigma_inc"], sample["temperature"]))

    outFile.write("# WAVEVECTOR vector of m={} values in Angstroem-1: q\n".format(len(q)))
    outFile.write(' '.join([str(qi) for qi in q])+"\n")
    outFile.write("# ENERGY vector of n={} values in meV: w\n".format(len(w)))
    outFile.write(' '.join([str(THztomeV*wi) for wi in w])+"\n")
    outFile.write("# matrix of S(q,w) values (m rows x n columns,{}x{}), one line per q value: sqw\n".format(len(q),len(w)))
    for qrow in sqw:
        outFile.write(' '.join([str(s) for s in qrow])+"\n")
    outFile.write("# end of Sqw file\n")

# plot S(q,w) in 2D
fig,ax=plt.subplots()
xy_limits_for_plot=[min(w),max(w),max(q),min(q)]

im=ax.imshow(sqw,aspect='auto',extent=xy_limits_for_plot)
cbar=ax.figure.colorbar(im,ax=ax)
cbar.ax.set_ylabel("S(q,w)",rotation=-90,va="bottom")
ax.set_xlabel("w [1/ps]")
ax.set_ylabel("q [1/AA]")
ax.invert_yaxis()
plt.savefig(sample["name"]+"_sqw_2d.png")

# plot S(q,w) in 1D
fig = plt.figure(figsize=(15,10))

ax1 = fig.add_subplot(121)
for i in range(q.size):
    if i%100==0:
        integral = np.trapz(sqw[i,:], w)
        print ("Integral S(q=",q[i],",w) = ", integral)
        ax1.plot(w, sqw[i,:])
ax1.grid(True)
ax1.set_xlabel('w [1/ps]', fontsize=20)
ax1.set_ylabel('S(q,w)', fontsize=20)
ax1.set_yscale('log')
ax1.tick_params(labelsize=16)

ax2 = fig.add_subplot(122)
for i in range(q.size):
    if i%100==0:
        ax2.plot(w, sqw[i,:], label="q={:.2f}".format((q[i])))
ax2.grid(True)
ax2.set_xlabel('w [1/ps]', fontsize=20)
ax2.set_ylabel('S(q,w)', fontsize=20)
ax2.tick_params(labelsize=16)
ax2.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

plt.tight_layout()
plt.suptitle('S(q,w) log and linear scales',x=0.55,y=1.025, fontsize=20)
plt.savefig(sample["name"]+"_sqw_1d.png")

plt.show()   



