"""Utility functions used across multiple files.

"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# DATA PARSING

def parse_sqw(filename, q_label, w_label, sqw_label):
    """Parse a .sqw file into a DataFrame."""
    with open(filename) as f:
        line = f.readline()
        line = f.readline()
        while line[0]=='#':
            line=f.readline()
        #line should now not be '#' started
        q = line.strip().split()
        q = [float(qi) for qi in q]
        line = f.readline()
        while line[0]=='#':
            line=f.readline()
        w = line.strip().split()
        w = [float(wi) for wi in w]
        line = f.readline()
        while line[0]=='#':
            line=f.readline()

        sqw_list = []
        sqw_row_count = -1
        while line!="" and line[0]!='#':
            sqw_row_count += 1
            qi = q[sqw_row_count]
            s = line.strip().split()
            s = [float(si) for si in s]
            for j in range(len(s)):
                sqw_list.append([qi,w[j],s[j]])
            line=f.readline()
    
    df = pd.DataFrame(sqw_list, columns=[q_label,w_label,sqw_label])
    return df

def parse_E_th_file(filename):
    """Parse a McStas detector output file into a dictionary."""
    info={"Param" : {}}
    with open(filename) as f:
        line=f.readline()
        while line != '':
            if line[0]=="#":
	            #handle 'comment' line - extract parameters
                line=line.split(":",1)
                line=[l.strip('#').strip() for l in line]
                if line[1]=='':
                    #if no parameter value given, assume
                    #corresponding data continues on subsequent 
                    #uncommented lines
                    raw_data=""
                    data_line=f.readline()
                    #read until end of this set of data
                    while data_line != '' and data_line[0] != '#':
                        raw_data+=data_line
                        data_line=f.readline()
                    #preserve breaking line for next loop
                    info[line[0]] = raw_data
                    line=data_line
                    continue
                elif line[0]=="Param":
                    #multiple parameters are given with the same prefix, so put these into their own sub-dictionary
                    param_info = line[1].split('=')
                    info["Param"][param_info[0]] = param_info[1]
                else:
                    #if parameter value is given,
                    #put straight into dictionary
                    info[line[0]] = line[1]

            line=f.readline()
    return info

def parse_E_th_data(raw_data, xy_lims, xvar, yvar, zvar):
    """Parse one chunk of data (e.g. intensity values) from a McStas detector file into a DataFrame.

    Guesses the bin sizes based on the limits on the range of the X and Y variables and the size of the data, assuming that the data is binned linearly.
    """
    data_rows=raw_data.split("\n")
    data=[row.split(" ")[:-1] for row in data_rows[:-1]] #cuts off final empty entries of row and data
    
    #parse limits
    xy_lims_split = xy_lims.split(" ")
    x_lims = [float(xy_lims_split[0]), float(xy_lims_split[1])]
    y_lims = [float(xy_lims_split[2]), float(xy_lims_split[3])]

    x = np.linspace(x_lims[0], x_lims[1], len(data[0]))
    y = np.linspace(y_lims[0], y_lims[1], len(data))

    #create dataframe format via unlabelled 2d table
    df = pd.DataFrame(data,columns=y,index=x)
    N,K = df.shape
    data2 = {zvar: df.to_numpy().ravel(), yvar: np.asarray(df.columns).repeat(N), xvar: np.tile(np.asarray(df.index),K)}
    df2 = pd.DataFrame(data2, columns=[xvar,yvar,zvar])
    df2[zvar] = df2[zvar].astype(float)
    
    return df2

def generate_df_from_E_th_data(info):
    """Parses a McStas detector file and returns a DataFrame.
    
    Assumes that the names of the data chunks in the McStas detector file start with 'Data', 'Errors' and 'Events'."""
    x_label = info['xlabel']
    y_label = info['ylabel']
    z_label = info['zlabel']
    err_label = z_label+'_err'
    n_label = "Number of events"

    data_key = find_key(info, "Data")[0]
    err_key = find_key(info, "Errors")[0]
    n_key = find_key(info,"Events")[0]

    main_data = parse_E_th_data(info[data_key], info['xylimits'], x_label, y_label, z_label)
    err_data = parse_E_th_data(info[err_key], info['xylimits'], x_label, y_label, err_label)
    n_data = parse_E_th_data(info[n_key], info['xylimits'], x_label, y_label, n_label)

    #combine all data into one DataFrame
    main_data[err_label] = err_data[err_label]
    main_data[n_label] = n_data[n_label]

    return main_data

# MISCELLANEOUS

def get_name_from_path(path_to_file):
    """Return the name of a file without its path or extension.

    Example: '/path/to/file.ext' -> 'file'
    """
    return os.path.splitext(os.path.basename(path_to_file))[0]

def find_key(dictionary, start):
    """Find a key in a dictionary given the start of its string."""
    ret = []
    for key in dictionary:
        if key.startswith(start):
            ret.append(key)
    return ret

def convert_to_q_w(data, E0, longitude_label, energy_label, q_label, w_label):
    """Takes a DataFrame containing data in E vs. longitude, and adds columns for the corresponding values of q and w.
    """
    c=3e8 #m/s
    m=9.39565e11 #neutron mass meV/c^2
    hbar=6.582e-13 #meV s
    m2AA=1e10 #conversion metres to Angstroems
    #hbar^2q^2/2m = E_i + E_f - 2*sqrt(E_i*E_f)*cos(longitude)
    data[q_label] = np.sqrt((2*m/(c**2*hbar**2))*(data[energy_label]+E0-2*np.sqrt(E0*data[energy_label])*np.cos(data[longitude_label]*2*np.pi/360.)))/m2AA #1/AA
    data[w_label] = (data[energy_label] - E0) #meV
    return data

def round_q(data, q_label, step_width, q_rounded_label):
    """Takes a DataFrame and a column to round to a certain size (step_width, e.g. 0.1). Adds a column to the DataFrame with the corresponding rounded values.
    """
    step_factor = int(1/step_width)
    data[q_rounded_label] = round(data[q_label]*step_factor)/step_factor
    return data

def symmetrize_w(data, w_label):
    """Given a DataFrame and a specific column within that DataFrame, choose a subset of the DataFrame such that that column contains an equal number of unique values above and below 0.

    Assumes the column contains some negative values to begin with.

    Example: if input column contains unique values {-3,-2,-1,0,1,2,3,4,5}, output is the DataFrame subset where that column contains unique values {-3,-2,-1,0,1,2,3}.
    """
    w_vals = np.sort(data[w_label].unique())
    num_negative_vals = len(np.where(w_vals<0)[0])
    if (np.where(w_vals==0)[0]!=np.array([])):
        w_vals = w_vals[0:num_negative_vals*2+1]
    else:
        w_vals = w_vals[0:num_negative_vals*2]

    data = data[data[w_label].between(min(w_vals),max(w_vals),inclusive=True)]

    return data

# PLOTTING

def plot_2d_from_dataframe(df, x_label, y_label, z_label, filename="", fillna=0, title="", x_axlabel="", y_axlabel=""):
    """Plot a 2D heatmap of intensity data from a DataFrame.
    
    The X and Y axis ticks are assumed to be linearly spaced between the upper and lower limits of the X and Y values in the DataFrame. This will produce incorrect axis ticks if the data is not linearly spaced.
    """
    fig,ax=plt.subplots()
    pivot = df.pivot_table(index=y_label, columns=x_label, values=z_label)
    if fillna != np.nan:
        pivot=pivot.fillna(value=fillna)
    xy_limits_for_plot = [min(pivot.columns), max(pivot.columns), max(pivot.index), min(pivot.index)]
    im = ax.imshow(pivot, aspect='auto', extent=xy_limits_for_plot)
    ax.invert_yaxis() #necessary in this context
    if x_axlabel!="":
        ax.set_xlabel(x_axlabel)
    else:
        ax.set_xlabel(pivot.columns.name)
    if y_axlabel!="":
        ax.set_ylabel(y_axlabel)
    else:
        ax.set_ylabel(pivot.index.name)
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel(z_label, rotation=-90, va="bottom")
    if title!="":
        ax.set_title(title)
    if filename!="":
        plt.savefig(filename,dpi=300)
